//
//  SPI.h
//  Lection2
//
//  Created by Admin on 29.10.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

typedef  NS_ENUM(NSUInteger,SPIStarSystemStarKind) {
RedGiant = 1,
WhiteGnome,
Neutrino,
DoubleStar,}
;

@interface SPIStar : SPISpaceObject



@property (nonatomic, assign/*, setter = setKind*/) SPIStarSystemStarKind currentStarKind;
@property (nonatomic, strong,setter = setStarMass:) NSNumber *StarMass;
@property (nonatomic, assign) BOOL IsDealloc;

//-(instancetype)initWithName:(NSString*) name;
-(instancetype)initWithName:(NSString *)name CanBeDestroied: (BOOL)IsDestroy;

@end
