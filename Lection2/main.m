//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"
#import "SPIStar.h"

int main(int argc, const char * argv[]) {
    
    
    // первая зыездная система
    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
    
    SPIStar *Zentavra = [[SPIStar alloc] initWithName:@"AlphaZentavra"  CanBeDestroied: YES];
    Zentavra.currentStarKind = (NSUInteger)DoubleStar;
    //Zentavra.StarMass  = @2.7;
    
    
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan" CanBeDestroied: NO];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 325000000;

    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota" CanBeDestroied: YES];
    hotaAsteroidField.density = 6000000;
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey" CanBeDestroied: NO];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo" CanBeDestroied:NO];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto" CanBeDestroied:YES];
    plutoPlanet.atmosphere = NO;
    
    
    alphaStarSystem.spaceObjects = [NSMutableArray arrayWithArray:@[Zentavra, vulkanPlanet, hotaAsteroidField, gallifreiPlanet, nabooPlanet, plutoPlanet]];
    
    //   вторая звездная система
    
    SPIStarSystem *OrionStarSystem = [[SPIStarSystem alloc] initWithName:@"Orion" age:@(6000000000)];
    
    SPIStar * AltsionaStar = [[SPIStar alloc] initWithName:@"Altsiona" CanBeDestroied: YES];
    AltsionaStar.currentStarKind = (NSUInteger)RedGiant;
    
    SPIPlanet * ColdPlanet = [[SPIPlanet alloc] initWithName:@"Cold" CanBeDestroied: YES];
    ColdPlanet.atmosphere = YES;
    ColdPlanet.peoplesCount = 45600000;
    
    SPIAsteroidField *GalleyAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Galley" CanBeDestroied: YES];
    GalleyAsteroidField.density = 3400000;
    
    OrionStarSystem.spaceObjects = [NSMutableArray arrayWithArray:@[AltsionaStar,ColdPlanet,GalleyAsteroidField]];
    
    //  третья звездная система
    
    
    SPIStarSystem *MagellanStarSystem = [[SPIStarSystem alloc] initWithName:@"Magellan" age:@(45000000000)];
    
    SPIStar * LionStar = [[SPIStar alloc] initWithName:@"Lion" CanBeDestroied: YES];
    LionStar.currentStarKind = (NSUInteger)WhiteGnome;
    
    SPIPlanet * SandPlanet = [[SPIPlanet alloc] initWithName:@"SandPlanet" CanBeDestroied: YES];
    SandPlanet.atmosphere = YES;
    SandPlanet.peoplesCount = 9800000;
    
    SPIAsteroidField *E123AsteroidField = [[SPIAsteroidField alloc] initWithName:@"E123" CanBeDestroied: YES];
    E123AsteroidField.density = 1200000;
    
    MagellanStarSystem.spaceObjects = [NSMutableArray arrayWithArray:@[LionStar,SandPlanet,E123AsteroidField]];
    
    
    
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Falcon"];
    //[spaceship loadStarSystem:alphaStarSystem];
    
    BOOL play = YES;
    
    NSMutableDictionary *starObjects = [[NSMutableDictionary alloc] init];
    [starObjects setObject:alphaStarSystem forKey:@"s"];
    [starObjects setObject:OrionStarSystem forKey:@"o"];
    [starObjects setObject:MagellanStarSystem forKey:@"m"];
    
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:OrionStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:MagellanStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];
 
    [spaceship loadStarSystem:starObjects[@"o"]];
    NSString* StarObjName = @"o";
    // alphaStarSystem.spaceObjects
    NSInteger index = [((SPIStarSystem*)starObjects[@"o"]).spaceObjects indexOfObject:spaceship.currentSpaceObject];

    while (play) {
        
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        if (response == SPIPlayerSpaceshipResponseMove)
        {   char instruct[1];
            printf("\n%s ",[[spaceship GetCurrentSystem] cStringUsingEncoding:NSUTF8StringEncoding]);
            printf("\n%s ",[@"Press the key 's' to move to Sirius, 'o' to move to Orion, 'm' to move to Magellan: " cStringUsingEncoding:NSUTF8StringEncoding]);
            
            fgets(instruct, 255, stdin);
            if (([[NSString stringWithFormat:@"%c", *instruct] isEqualToString:(NSString*) @"o"])||([[NSString stringWithFormat:@"%c", *instruct] isEqualToString:(NSString*)@"m"])||
                ([[NSString stringWithFormat:@"%c", *instruct] isEqualToString:(NSString*)@"s"]))
            { [spaceship loadStarSystem:starObjects[[NSString stringWithFormat:@"%c", *instruct]]];
            index = [((SPIStarSystem*)starObjects[[NSString stringWithFormat:@"%c", *instruct]]).spaceObjects indexOfObject:spaceship.currentSpaceObject];
                StarObjName = [NSString stringWithFormat:@"%c", *instruct];}
            else
            { printf("\nИнструкция'%c' не существует!",*instruct);}
            
            
        }
        
         index = [((SPIStarSystem*)starObjects[StarObjName]).spaceObjects indexOfObject:spaceship.currentSpaceObject];
        
        if (response == SPIPlayerSpaceshipResponseDestroy)
        {
           if (spaceship.currentSpaceObject.IsDestroy == YES) {
               if (spaceship.currentSpaceObject.type == SPISpaceObjectTypeAsteroidField){
               [((SPIStarSystem*)starObjects[StarObjName]).spaceObjects removeObjectAtIndex: index];
                   //((SPIStarSystem*)starObjects[StarObjName]).spaceObjects.count
                 if (((SPIStarSystem*)starObjects[StarObjName]).spaceObjects.count != 0)
                    {
                   if (index < ((SPIStarSystem*)starObjects[StarObjName]).spaceObjects.count-1)
                   {
                       spaceship.currentSpaceObject = [spaceship.starSystem.spaceObjects objectAtIndex:index + 1];
                   }
                   if ((index > 0 ) && (index == ((SPIStarSystem*)starObjects[StarObjName]).spaceObjects.count))
                   {
                       spaceship.currentSpaceObject = [spaceship.starSystem.spaceObjects objectAtIndex:index - 1];
                   }
                   if ( ((SPIStarSystem*)starObjects[StarObjName]).spaceObjects.count == 1)
                   {
                       spaceship.currentSpaceObject = [spaceship.starSystem.spaceObjects objectAtIndex:0];
                   }
                 }
                   

               }
               else
               {
               SPISpaceObject* obj  = [spaceship.currentSpaceObject BecameAnotherObject];
               [((SPIStarSystem*)starObjects[StarObjName]).spaceObjects replaceObjectAtIndex: index withObject: obj ];
               spaceship.currentSpaceObject =obj;
               }
           }
            else
            {
                printf("\nObject can't be destroyed!");
                
            }
               
            
            }
          
        
        for (id<SPIGameObject> gameObject in gameObjects) {
           if (((SPIStarSystem*)starObjects[StarObjName]).spaceObjects.count != 0)
           {[gameObject nextTurn];
           }
        }
            
    
    }
    return 0;
}

//spaceship.currentSpaceObject
