//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)loadStarSystem:(SPIStarSystem *)starSystem {
    _starSystem = starSystem;
    _currentSpaceObject = starSystem.spaceObjects.firstObject; //_starSystem.spaceObjects.firstObject;
}
- (NSString *)GetCurrentSystem{
    return [NSString stringWithFormat:@"\nYou are in Star System %@ ", _starSystem.name];
}

- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];
    SPIPlayerSpaceshipResponse res;
    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    int commandNumber = atoi(command);
    self.nextCommand = commandNumber;
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandExit:
            res = SPIPlayerSpaceshipResponseExit;
            break;
        case SPIPlayerSpaceshipCommandFlyToOptionalObject:
            res = SPIPlayerSpaceshipResponseMove;
            break;
        case SPIPlayerSpaceshipCommandDestroyObject:
            res = SPIPlayerSpaceshipResponseDestroy;
            break;

        default:
            res = SPIPlayerSpaceshipResponseOK;
            break;
    }
    return  res ; //self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (void)nextTurn {
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
            break;
        }
            
        default:
            break;
    }
    self.nextCommand = 0;
}

- (id)BecameAnotherObject{
    return @"111";
}

- (NSString *)availableCommands {
    if (self.starSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandDestroyObject; command++) {
            NSString *description = [self descriptionForCommand:command];
            if (description) {
                [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
            }
        }
        
        NSString *description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if ((currentSpaceObjectIndex > 0) && (self.starSystem.spaceObjects.count != 0)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            if (self.starSystem.spaceObjects.count != 0){
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if ((currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) && (self.starSystem.spaceObjects.count != 0) ) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            break;
        }
         
        case SPIPlayerSpaceshipCommandFlyToOptionalObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if ((currentSpaceObjectIndex == (self.starSystem.spaceObjects.count - 1)) || (self.starSystem.spaceObjects.count == 0))
            {
                commandDescription = [NSString stringWithFormat:@"Move to another space system "];
            }
            break;
        }
    
            
        case  SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@""];
            break;
        }
        case  SPIPlayerSpaceshipCommandDestroyObject: {
            if (self.starSystem.spaceObjects.count != 0) {
            commandDescription = [NSString stringWithFormat:@"Destroy current object!"];
            }
            break;
        }
    
            
        default:
            break;
    }
    return commandDescription;
}


@end
