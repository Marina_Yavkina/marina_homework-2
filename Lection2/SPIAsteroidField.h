//
//  AsteroidField.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 10/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface SPIAsteroidField : SPISpaceObject

@property (nonatomic,assign) NSInteger density;
@property (nonatomic) BOOL IsDealloc;

- (instancetype)initWithName:(NSString *)name CanBeDestroied: (BOOL)IsDestroy;

@end
