//
//  Planet.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 10/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"

@implementation SPIPlanet

- (instancetype)initWithName:(NSString *)name CanBeDestroied: (BOOL)IsDestroy {
    self = [super initWithType:SPISpaceObjectTypePlanet name:name CanBeDestroied: IsDestroy];
    if (self) {
        
    }
    return self;
}

- (void)nextTurn {
    [super nextTurn];
    
    if (self.hasPeoples) {
        self.peoplesCount += ((int)(arc4random() % 1000)) - 200;
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nPlanet: %@,\nHas atmosphere: %@ \nHas peoples: %@ \nPeoples count: %ld\nIs destroyed? %@",
            self.name,
            self.hasAtmosphere ? @"YES" : @"NO",
            self.hasPeoples ? @"YES" : @"NO",
            self.peoplesCount,
            self.IsDestroy ? @"YES" : @"NO"];
}


- (SPIAsteroidField*) BecameAnotherObject{
    
    SPIAsteroidField * NewObject  = [[SPIAsteroidField alloc] initWithName:[@"Asteriod_" stringByAppendingString: self.name] CanBeDestroied: YES];
    NewObject.density = ((int)(arc4random() % 10000000));
    return NewObject;
    
}


- (BOOL)hasPeoples {
    return self.peoplesCount > 0;
}





@end
