//
//  SPIGameObject.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 28/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SPIGameObject <NSObject>

- (void)nextTurn;
- (id)BecameAnotherObject;
@end
