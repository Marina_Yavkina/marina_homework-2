//
//  SPI.m
//  Lection2
//
//  Created by Admin on 29.10.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIStar.h"
#import "SPIPlanet.h"

@implementation SPIStar

- (void)setStarMass:(NSNumber *)StarMass  {
    _StarMass = [NSNumber numberWithFloat:3.67];
}

/*- (void)setKind:( SPIStarSystemStarKind *) val {
    _StarMass = [[SPIStarSystemStarKin alloc] initwithName: (SPIStarSystemStarKind *)val];*/
//}

+ (NSString *)typeStringForType:(SPIStarSystemStarKind ) mass {
    
    switch (mass) {
        case RedGiant:
            return @"RedGiant";
            
        case WhiteGnome:
            return @"White Gnome";
        case Neutrino:
            return @"Neutrino";
        case DoubleStar:
            return @"DoubleStar";
            
        default:
        return @"Unknown";
    }
}

- (instancetype)initWithName:(NSString *)name CanBeDestroied: (BOOL)IsDestroy;{
    self = [super initWithType:SPISpaceObjectTypeStar name:name  CanBeDestroied: IsDestroy];
    if (self) {
        
    }
    return self;
}

- (void) dealloc{
    
  
}

- (void)nextTurn {
    [super nextTurn];
    
    self.StarMass =  @((arc4random() % 100)/10);
}

- (NSString *)description {
    //NSLog(@"%@", self.name);
    //NSLog(@"%@", self.StarMass);
    
    //return @"";
    
    return [NSString stringWithFormat:@"\nStarName: %@\nKindStar: %@\nStar Mass: %@\nIs destroyed? %@", self.name,[SPIStar typeStringForType: self.currentStarKind], self.StarMass,self.IsDestroy ? @"YES" : @"NO"];
}
- (SPIPlanet*) BecameAnotherObject{
    
    SPIPlanet *NewObject = [[SPIPlanet alloc] initWithName:[@"Planet_" stringByAppendingString: self.name] CanBeDestroied:YES];
    NewObject.atmosphere = YES;
    NewObject.peoplesCount = 625000000;
    return NewObject;
    
}


@end
