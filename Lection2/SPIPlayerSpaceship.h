//
//  PlayerSpaceship.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"
#import "SPIStarSystem.h"
#import "SPIGameObject.h"

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipCommand) {
    SPIPlayerSpaceshipCommandFlyToPreviousObject = 1,
    SPIPlayerSpaceshipCommandExploreCurrentObject,
    SPIPlayerSpaceshipCommandFlyToNextObject,
    SPIPlayerSpaceshipCommandFlyToOptionalObject,
    SPIPlayerSpaceshipCommandDestroyObject,
    SPIPlayerSpaceshipCommandExit = 42,
};

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipResponse) {
    SPIPlayerSpaceshipResponseOK = 0,
    SPIPlayerSpaceshipResponseExit,
    SPIPlayerSpaceshipResponseMove,
    SPIPlayerSpaceshipResponseDestroy,
};



@interface SPIPlayerSpaceship : NSObject <SPIGameObject>

@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, assign) SPIPlayerSpaceshipCommand nextCommand;
@property (nonatomic, weak, setter=loadStarSystem:) SPIStarSystem *starSystem;
@property (nonatomic, weak) SPISpaceObject *currentSpaceObject;



- (instancetype)initWithName:(NSString *)name;
- (NSString *)GetCurrentSystem;
- (NSString *)availableCommands;


- (SPIPlayerSpaceshipResponse)waitForCommand;

@end
