//
//  AsteroidField.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 10/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIAsteroidField.h"

@implementation SPIAsteroidField

- (instancetype)initWithName:(NSString *)name CanBeDestroied: (BOOL)IsDestroy {
    self = [super initWithType:SPISpaceObjectTypeAsteroidField name:name CanBeDestroied: IsDestroy];
    if (self) {
        
    }
    return self;

}

- (void)nextTurn {
    [super nextTurn];
    self.density += ((int)(arc4random() % 100)) - 50;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nAsteroid field: %@\nDensity: %ld\nIs destroyed? %@", self.name, self.density,self.IsDestroy ? @"YES" : @"NO"];
}

- (NSString*) BecameAnotherObject{
    
  return [NSString stringWithFormat:@"\nObject deleted from star system!"];
}

@end
